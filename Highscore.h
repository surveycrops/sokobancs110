#pragma once

#include <stdio.h>
#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>

using namespace sf;
using namespace std;
class highscore
{
public:

	highscore();
	string getName();
	int getScore();
	void nameEntry(RenderWindow&, Clock&, Event&, int&);
	void getData();
	void addUser(string, int);
	void displayHighScore(RenderWindow&);
	bool get = true;

private:
	string name;
	int Score;
	string userName;
	struct players
	{
		string name;
		int level;
	};

	vector<players>users;
	void sort();
};