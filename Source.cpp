#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <string>
#include "player.h"
#include "block.h"
#include "tmaps.h"
#include "Highscore.h"
using namespace std;

//====================================================================================================================================

int main()
{	
//	int numhs = 1;
//	int levels = 3;
//	highscore highscores1;

	bool MenuOn = true;					//Start Menu is displayed and functioning
	bool MenuOff = false;				//Start Menu is off and game has begun
	bool CreditsOn = false;				//Credits screen is on
//	bool ScoresOn = false;				//High Scores screen is on

	sf::Music menumusic;				//Background Music
	if (!menumusic.openFromFile("backmusic.wav"))
		return EXIT_FAILURE;

	sf::Font menufont;					//Start Menu font
	if (!menufont.loadFromFile("airstrikechrome.ttf"))
		return EXIT_FAILURE;

	sf::Text choice;					//Start Menu Options text
	choice.setCharacterSize(50);
	choice.setColor(Color::White);
	choice.setFont(menufont);
	int menucounter = 0;

	int count = 0;						//Number of steps
	string s = "Steps: ";
	int level = 1;						//Level Number
	bool flag1 = false;					//Block 1 has reached its correct position
	bool flag2 = false;					//Block 2 has reached its correct position
	bool lvlstart = true;				//New level has just started
	
	player player1;						//Player
	sf::Texture texture;				//Player's sprite
	player1.sprite.setTexture(texture);
	if (!texture.loadFromFile("Spritesheet.png"))
		return EXIT_FAILURE;

	block block1;						//Block 1
	sf::Texture btexture;
	block1.blocksprite.setTexture(btexture);
	if (!btexture.loadFromFile("block.png"))
		return EXIT_FAILURE;

	block block2;						//Block 1
	block2.blocksprite.setTexture(btexture);
	if (!btexture.loadFromFile("block.png"))
		return EXIT_FAILURE;

	sf::Sprite pos1;					//Target position 1
	pos1.setTexture(btexture);
	if (!btexture.loadFromFile("block.png"))
		return EXIT_FAILURE;
	pos1.setTextureRect(sf::IntRect(0, 0, 32, 32));

	sf::Sprite pos2;					//Target position 2
	pos2.setTexture(btexture);
	if (!btexture.loadFromFile("block.png"))
		return EXIT_FAILURE;
	pos2.setTextureRect(sf::IntRect(0, 0, 32, 32));

	sf::Texture backgroundtexture;		//Start Menu Background
	if (!backgroundtexture.loadFromFile("background.png"))
	{
		return EXIT_FAILURE;
	}
	sf::Sprite background(backgroundtexture);

	sf::Font font;						//Game Text font
	if (!font.loadFromFile("timesnewarial.ttf"))
	{
		cout << "error";
	}

	sf::Text goback;					//Go back from credits or highscores to start menu
	goback.setFont(menufont);
	goback.setCharacterSize(15);
	goback.setPosition(250, 450);
	goback.setColor(sf::Color::White);
	goback.setString("Press space to go back");

	sf::Text credit;					//Text in credits screen
	credit.setFont(menufont);
	credit.setCharacterSize(40);
	credit.setPosition(10, 10);
	credit.setColor(sf::Color::White);
	credit.setString(" Zeyad Elbadrawy \n \n Mina Labib \n \n Marwan Marwan \n \n Abdelrahman \n Hassan");

	sf::Text num;						//Number of steps
	num.setFont(font);
	num.setCharacterSize(25);
	num.setPosition(420, 10);
	num.setColor(sf::Color::White);

	sf::Text lvl1;						//Min. number of steps in level 1
	lvl1.setFont(font);
	lvl1.setCharacterSize(25);
	lvl1.setPosition(10, 300);
	lvl1.setColor(sf::Color::White);
	lvl1.setString("Minimum number of steps is 5");

	sf::Text lvl2;						//Min. number of steps in level 2
	lvl2.setFont(font);
	lvl2.setCharacterSize(25);
	lvl2.setPosition(10, 300);
	lvl2.setColor(sf::Color::White);
	lvl2.setString("Minimum number of steps is 34");

	sf::Text lvl3;						//Min. number of steps in level 3
	lvl3.setFont(font);
	lvl3.setCharacterSize(25);
	lvl3.setPosition(10, 300);
	lvl3.setColor(sf::Color::White);
	lvl3.setString("Minimum number of steps is 18");

	sf::Text lvl4;						//Min. number of steps in level 4
	lvl4.setFont(font);
	lvl4.setCharacterSize(25);
	lvl4.setPosition(10, 300);
	lvl4.setColor(sf::Color::White);
	lvl4.setString("Minimum number of steps is 45");

	sf::Text lvl5;						//Min. number of steps in level 5
	lvl5.setFont(font);
	lvl5.setString("Minimum number of steps is 87");
	lvl5.setCharacterSize(25);
	lvl5.setPosition(10, 300);
	lvl5.setColor(sf::Color::White);

	sf::Text counter;					//Steps:
	counter.setFont(font);
	counter.setString("Steps: ");
	counter.setCharacterSize(25);
	counter.setPosition(350, 10);
	counter.setColor(sf::Color::White);

	sf::Text ins;						//Instructions
	ins.setFont(font);
	ins.setString("Push boxes to gray areas. Press B to restart");
	ins.setCharacterSize(25);
	ins.setPosition(10, 400);
	ins.setColor(sf::Color::White);

	sf::Text win;						//Winner text
	win.setFont(font);
	win.setString("You win! Press space to continue");
	win.setCharacterSize(25);
	win.setPosition(10, 450);
	win.setColor(sf::Color::White);

	sf::Text fwin;						//Winner of final level text
	fwin.setFont(font);
	fwin.setString("You beat all five levels! Congratulations!");
	fwin.setCharacterSize(25);
	fwin.setPosition(10, 450);
	fwin.setColor(sf::Color::White);

	sf::Time time;
	sf::Clock clock;	

	menumusic.play();			//Play Music
//====================================================================================================================================

	sf::RenderWindow window(sf::VideoMode(500, 500), "Sokoban");
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape || event.type == Event::Closed)
				window.close();
		}
		window.clear();

		if (MenuOn)						//If start menu is on
		{
			window.draw(background);
			window.draw(choice);
			time = clock.getElapsedTime();
			if (time.asMilliseconds() >= 500);
			{				//Press Left/Up or Right/Down to scroll through menu
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
					menucounter--;
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)))
					menucounter++;

				clock.restart();
			}

			if (menucounter < 0)
				menucounter = 450;
			else if (menucounter > 450)
				menucounter = 0;

			//Display first option
			if (menucounter >= 0 && menucounter <= 150)
			{
				choice.setString("START");
				choice.setPosition(150, 200);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
				{
					MenuOn = false;
					MenuOff = true;
				}
			}

			//Display second option
/*			if (menucounter > 150 && menucounter <= 300)
			{
				choice.setString("Highscores");
				choice.setPosition(100, 200);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
				{
					MenuOn = false;
					ScoresOn = true;
				}
			}
*/
			//Display third option
			if (menucounter > 150 && menucounter <= 300)
			{
				choice.setString("CREDITS");
				choice.setPosition(120, 200);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
				{
					CreditsOn = true;
					MenuOn = false;
				}
			}

			//Display fourth option
			if (menucounter > 300 && menucounter <= 450)
			{
				choice.setString("EXIT");
				choice.setPosition(150, 200);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
				{
					window.close();
					return 0;
				}
			}
		}
		if (CreditsOn)		//Display credits
		{
			window.draw(credit);
			window.draw(goback);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				MenuOn = true;
				CreditsOn = false;
			}
		}
/*
		sf::Clock clock1;

		if (ScoresOn)
		{
			highscores1.addUser(highscores1.getName(), levels);

			if (numhs == 1)
				highscores1.nameEntry(window, clock1, event, numhs);

			else if (numhs == 2)
			{

			}

			else if (numhs == 5)
			{
				highscores1.displayHighScore(window);
			}

			window.draw(goback);;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				MenuOn = true;
				ScoresOn = false;
			}

		}
		*/
//====================================================================================================================================
		//Begin Game
		if (MenuOff)
		{
			//Restart level
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
			{
				flag1 = false;
				flag2 = false;
				lvlstart = true;
				count = 0;
				player1.sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
			}

			tilemaps tilesobject(level);
			//Draw Level
			for (int i = 0; i < tilesobject.getloadcounter().x; i++)
				for (int j = 0; j < tilesobject.getloadcounter().y; j++)
					window.draw(tilesobject.displaymap(i, j));

			if (level == 1)		//Initialize level 1 locations
			{
				window.draw(lvl1);
				if (lvlstart)
				{
					player1.sprite.setPosition(32, 128);
					block1.blocksprite.setPosition(32, 96);
					block2.blocksprite.setPosition(64, 96);
					pos1.setPosition(32, 32);
					pos2.setPosition(128, 96);
					count = 0;
					lvlstart = false;					
				}
			}

			if (level == 2)		//Initialize level 2 locations
			{
				window.draw(lvl2);
				if (lvlstart)
				{
					player1.sprite.setPosition(32, 32);
					block1.blocksprite.setPosition(160, 160);
					block2.blocksprite.setPosition(96, 64);
					pos1.setPosition(224, 128);
					pos2.setPosition(224, 96);
					count = 0;
					lvlstart = false;
				}
			}

			if (level == 3)		//Initialize level 3 locations
			{
				window.draw(lvl3);
				if (lvlstart)
				{
					player1.sprite.setPosition(128, 64);
					block1.blocksprite.setPosition(96, 64);
					block2.blocksprite.setPosition(64, 64);
					pos1.setPosition(128, 32);
					pos2.setPosition(128, 96);
					count = 0;
					lvlstart = false;
				}
			}

			if (level == 4)		//Initialize level 4 locations
			{
				window.draw(lvl4);
				if (lvlstart)
				{
					player1.sprite.setPosition(64, 96);
					block1.blocksprite.setPosition(64, 64);
					block2.blocksprite.setPosition(128, 96);
					pos1.setPosition(64, 160);
					pos2.setPosition(96, 160);
					count = 0;
					lvlstart = false;
				}
			}

			if (level == 5)		//Initialize level 5 locations
			{
				window.draw(lvl5);
				if (lvlstart)
				{
					player1.sprite.setPosition(192, 32);
					block1.blocksprite.setPosition(160, 64);
					block2.blocksprite.setPosition(128, 64);
					pos1.setPosition(128, 32);
					pos2.setPosition(160, 32);
					count = 0;
					lvlstart = false;
				}
			}

			//If level isn't complete, player can continue to move
			if (flag1 == false || flag2 == false)
			{
				time = clock.getElapsedTime();
				if (time.asMilliseconds() >= 150)
				{
					player1.moves(player1, block1, block2, tilesobject, pos1, pos2, flag1, flag2, count);
					clock.restart();
				}
			}
			//If any level other than last is complete, continue to next level
			else if (flag1 == true && flag2 == true && level != 5)
			{
				window.draw(win);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
				{
					level++;
					flag1 = false;
					flag2 = false;
					lvlstart = true;
				}
			}
			//if all levels are complete, return to start menu
			else if (flag1 == true && flag2 == true && level == 5)
			{
				window.draw(fwin);
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
				{
					MenuOff = false;
					MenuOn = true;
					level = 1;
					flag1 = false;
					flag2 = false;
					lvlstart = true;
				}
			}


			stringstream ss;
			ss << count;
			num.setString(ss.str().c_str());
			window.draw(num);
			window.draw(counter);
			window.draw(ins);
			window.draw(pos1);
			window.draw(pos2);
			window.draw(player1.sprite);
			window.draw(block1.blocksprite);
			window.draw(block2.blocksprite);
		}
		window.display();
	}
	return 0;
}