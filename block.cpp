#include "block.h"
#include "player.h"
#include "tmaps.h"
#include <SFML/Graphics.hpp>

block::block()
{
	blocksprite.setTextureRect(sf::IntRect(249.5, 56, 32, 32));
}

void block::bmoves(player& pp1, block& bb1, block& bb2, int d, tilemaps tilobject, int& cc)
{
	if (d == 0)
		for (int i = 0; i < 32; i++)
			bb1.blocksprite.move(0, -1);

	for (int k = 0; k < tilobject.getloadcounter().x; k++)
		for (int j = 0; j < tilobject.getloadcounter().y; j++)
			if (bb1.blocksprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()) || (bb1.blocksprite.getGlobalBounds().intersects(bb2.blocksprite.getGlobalBounds())))
			{
				for (int i = 0; i < 32; i++)
				{
					bb1.blocksprite.move(0, 1);
					pp1.sprite.move(0, 1);
				}
				cc--;
			}

	if (d == 1)
		for (int i = 0; i < 32; i++)
			bb1.blocksprite.move(0, 1);
	for (int k = 0; k < tilobject.getloadcounter().x; k++)
		for (int j = 0; j < tilobject.getloadcounter().y; j++)
			if (bb1.blocksprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()) || (bb1.blocksprite.getGlobalBounds().intersects(bb2.blocksprite.getGlobalBounds())))
			{
				for (int i = 0; i < 32; i++)
				{
					bb1.blocksprite.move(0, -1);
					pp1.sprite.move(0, -1);
				}
				cc--;
			}

	if (d == 2)
		for (int i = 0; i < 32; i++)
			bb1.blocksprite.move(-1, 0);
	for (int k = 0; k < tilobject.getloadcounter().x; k++)
		for (int j = 0; j < tilobject.getloadcounter().y; j++)
			if (bb1.blocksprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()) || (bb1.blocksprite.getGlobalBounds().intersects(bb2.blocksprite.getGlobalBounds())))
			{
				for (int i = 0; i < 32; i++)
				{
					bb1.blocksprite.move(1, 0);
					pp1.sprite.move(1, 0);
				}
				cc--;
			}

	if (d == 3)
		for (int i = 0; i < 32; i++)
			bb1.blocksprite.move(1, 0);
	for (int k = 0; k < tilobject.getloadcounter().x; k++)
		for (int j = 0; j < tilobject.getloadcounter().y; j++)
			if (bb1.blocksprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()) || (bb1.blocksprite.getGlobalBounds().intersects(bb2.blocksprite.getGlobalBounds())))
			{
				for (int i = 0; i < 32; i++)
				{
					bb1.blocksprite.move(-1, 0);
					pp1.sprite.move(-1, 0);
				}
				cc--;
			}
}