#include <SFML/Graphics.hpp>
#include "highscore.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>
#include <string>

highscore::highscore()
{
	name = "";
	Score = 0;
}

void highscore::sort()
{
	players temp;

	for (int i = 1; i < users.size(); i++)
		for (int j = 0; j < users.size() - i; j++)
		{

			// cout << users.at(i).name << endl;
			if (users.at(j).level < users.at(j + 1).level)
			{
				temp = users.at(j);
				users.at(j) = users.at(j + 1);
				users.at(j + 1) = temp;
			}
		}
}

void highscore::nameEntry(RenderWindow& window, Clock& clock, Event& event, int& num)
{
	Font font;

	if (!font.loadFromFile("sansation.ttf"))
		cout << "NO" << endl;

	sf::Text nameInput;
	nameInput.setCharacterSize(30);
	nameInput.setFont(font);
	nameInput.setString("Enter Name: ");
	nameInput.setPosition(0, 0);
	nameInput.setColor(Color::White);

	sf::Text theName;
	theName.setCharacterSize(30);
	theName.setFont(font);
	theName.setString(userName);
	theName.setPosition(0, 100);
	theName.setColor(Color::White);

	window.draw(nameInput);
	window.draw(theName);

	if (clock.getElapsedTime().asSeconds() >= 0.15)
	{
		clock.restart();
		if (event.type == sf::Event::TextEntered)
		{
			// Handle ASCII characters only
			if (event.text.unicode < 128)
			{
				cout << event.text.unicode << endl;
				if (event.text.unicode != 8)
					userName += static_cast<char>(event.text.unicode);
				else
					userName = userName.substr(0, userName.length() - 1);
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
		{
			transform(userName.begin(), userName.end(), userName.begin(), ::tolower);
			userName = userName.substr(0, userName.length() - 1);
			name = userName;
			num = 2;
		}
	}
}

string highscore::getName()
{
	return name;
}

int highscore::getScore()
{
	return Score;
}
void highscore::getData()
{
	ifstream input;

	input.open("user.txt");

	players player;

	while (!input.eof())
	{
		input >> player.name;
		input >> player.level;
		users.push_back(player);
	}

}

void highscore::displayHighScore(RenderWindow& window)
{
	Text highest[5];

	Font font;

	if (!font.loadFromFile("sansation.ttf"))
		cout << "NO" << endl;


	for (int i = 0; i<5; i++)
	{
		highest[i].setCharacterSize(30);
		highest[i].setFont(font);
		highest[i].setString(userName);
		highest[i].setPosition(20, i * 30 + 30);
		highest[i].setColor(Color::White);
	}
	if (get)
	{
		getData();
		get = false;
	}
	sort();
	for (int i = 0; i<5; i++)
	{
		highest[i].setString(users[i].name + "  " + to_string(users[i].level));
	}

	for (int i = 0; i<5; i++)
	{
		window.draw(highest[i]);
	}
}


void highscore::addUser(string name, int level)
{
	ofstream output;

	output.open("user.txt");
	if (output.fail())
		cout << "Failed to open output file" << endl;
	else
	{
		output << endl << name << " " << level << endl;
		get = true;
		output.close();
	}
}