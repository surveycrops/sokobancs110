#include "player.h"
#include "block.h"
#include "tmaps.h"
#include <SFML/Graphics.hpp>
using namespace sf;

player::player()
{
	sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
}

void player::moves(player& p1, block& b1, block& b2, tilemaps tilobject, Sprite ps1, Sprite ps2, bool& f1, bool& f2, int& c)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		p1.sprite.setTextureRect(sf::IntRect(0, 96, 32, 32));
			for (int i = 0; i < 32; i++)
				p1.sprite.move(0, -1);
		c++;

		for (int k = 0; k < tilobject.getloadcounter().x; k++)
			for (int j = 0; j < tilobject.getloadcounter().y; j++)
				if (p1.sprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()))
				{
					for (int i = 0; i < 32; i++)
						p1.sprite.move(0, 1);
					c--;
				}

		if (p1.sprite.getGlobalBounds().intersects(b1.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b1, b2, 0, tilobject, c);
		if (p1.sprite.getGlobalBounds().intersects(b2.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b2, b1, 0, tilobject, c);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		p1.sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
		for (int i = 0; i < 32; i++)
			p1.sprite.move(0, 1);
		c++;

		for (int k = 0; k < tilobject.getloadcounter().x; k++)
			for (int j = 0; j < tilobject.getloadcounter().y; j++)
				if (p1.sprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()))
				{
					for (int i = 0; i < 32; i++)
						p1.sprite.move(0, -1);
					c--;
				}

		if (p1.sprite.getGlobalBounds().intersects(b1.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b1, b2, 1, tilobject, c);
		if (p1.sprite.getGlobalBounds().intersects(b2.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b2, b1, 1, tilobject, c);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		p1.sprite.setTextureRect(sf::IntRect(0, 32, 32, 32));
		for (int i = 0; i < 32; i++)
			p1.sprite.move(-1, 0);
		c++;

		for (int k = 0; k < tilobject.getloadcounter().x; k++)
			for (int j = 0; j < tilobject.getloadcounter().y; j++)
				if (p1.sprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()))
				{
					for (int i = 0; i < 32; i++)
						p1.sprite.move(1, 0);
					c--;
				}
		if (p1.sprite.getGlobalBounds().intersects(b1.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b1, b2, 2, tilobject, c);
		if (p1.sprite.getGlobalBounds().intersects(b2.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b2, b1, 2, tilobject, c);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		p1.sprite.setTextureRect(sf::IntRect(0, 64, 32, 32));
		for (int i = 0; i < 32; i++)
			p1.sprite.move(1, 0);
		c++;

		for (int k = 0; k < tilobject.getloadcounter().x; k++)
			for (int j = 0; j < tilobject.getloadcounter().y; j++)
				if (p1.sprite.getGlobalBounds().intersects(tilobject.tilesarray[k*tilobject.getloadcounter().y + j].getGlobalBounds()))
				{
					for (int i = 0; i < 32; i++)
						p1.sprite.move(-1, 0);
					c--;
				}

		if (p1.sprite.getGlobalBounds().intersects(b1.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b1, b2, 3, tilobject, c);
		if (p1.sprite.getGlobalBounds().intersects(b2.blocksprite.getGlobalBounds()))
			b1.bmoves(p1, b2, b1, 3, tilobject, c);
	}

	if (b1.blocksprite.getGlobalBounds().intersects(ps1.getGlobalBounds()) || b1.blocksprite.getGlobalBounds().intersects(ps2.getGlobalBounds()))
		f1 = true;
	else
		f1 = false;
	if (b2.blocksprite.getGlobalBounds().intersects(ps1.getGlobalBounds()) || b2.blocksprite.getGlobalBounds().intersects(ps2.getGlobalBounds()))
		f2 = true;
	else
		f1 = false;
}