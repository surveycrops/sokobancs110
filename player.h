#pragma once
#include "block.h"
#include "tmaps.h"
#include <SFML/Graphics.hpp>

class block;
class player
{
public:
	player();
	sf::Sprite sprite;
	void moves(player&, block&, block&, tilemaps, Sprite, Sprite, bool&, bool&, int&);
};