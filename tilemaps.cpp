#include "tilemaps.h"
#include<SFML/Graphics.hpp>
#include<iostream>
#include<fstream>
#include<cctype>
#include<string>
using namespace std;
using namespace sf;

tilemaps::tilemaps()
{
	ifstream myfile("map1.txt");


	if (myfile.is_open())
	{
		string tilelocation;
		myfile >> tilelocation;
		tiletexture.loadFromFile(tilelocation);
		tiles.setTexture(tiletexture);

		while (!myfile.eof())
		{
			string str;
			myfile >> str;
			char x = str[0], y = str[2];

			if (!isdigit(x) || !isdigit(y))
				map[loadcounter.x][loadcounter.y] = Vector2i(-1, -1);
			else
				map[loadcounter.x][loadcounter.y] = Vector2i(x - '0', y - '0');

			if (myfile.peek() == '\n')
				loadcounter.x = 0, loadcounter.y++;
			else
				loadcounter.x++;

		}

		loadcounter.y++;
	}

	/*RenderWindow window(VideoMode(640, 540), "sokoban");

	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::KeyPressed&& event.key.code == Keyboard::Escape || event.type == Event::Closed)
				window.close();
		}
		window.clear(Color::Blue);

		for (int i = 0; i < loadcounter.x; i++)
		for (int j = 0; j < loadcounter.y; j++)
		if (map[i][j].x != -1 && map[i][j].y != -1)
		{
			tiles.setPosition(i * 32, j * 32);
			tiles.setTextureRect(IntRect(map[i][j].x * 32, map[i][j].y * 32, 32, 32));
			window.draw(tiles);
		}

		window.display();
	}*/
}

Sprite tilemaps::displaymap(int i, int j)
{

	for (i; i < loadcounter.x; i++){
		for (j; j < loadcounter.y; j++)
		{
			if (map[i][j].x != -1 && map[i][j].y != -1)
			{
				tiles.setPosition(i * 32, j * 32);
				tiles.setTextureRect(IntRect(map[i][j].x * 32, map[i][j].y * 32, 32, 32));

			}
			tilesarray[i*loadcounter.y+j] = tiles;


			return tiles;
		}
	}
}

Sprite* tilemaps::gettiles()
{
	return tilesarray;
}

Vector2i tilemaps::getloadcounter()
{
	return loadcounter;
}