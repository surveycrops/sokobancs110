#include "tmaps.h"
#include<SFML/Graphics.hpp>
#include<iostream>
#include<fstream>
#include<cctype>
#include<string>
using namespace std;
using namespace sf;

tilemaps::tilemaps(int level)
{
	if (level == 1)
		lname = "map1.txt";
	if (level == 2)
		lname = "map2.txt";
	if (level == 3)
		lname = "map3.txt";
	if (level == 4)
		lname = "map4.txt";
	if (level == 5)
		lname = "map5.txt";

	ifstream myfile(lname);

	if (myfile.is_open())
	{
		string tilelocation;
		myfile >> tilelocation;
		tiletexture.loadFromFile(tilelocation);
		tiles.setTexture(tiletexture);

		while (!myfile.eof())
		{
			string str;
			myfile >> str;
			char x = str[0], y = str[2];

			if (!isdigit(x) || !isdigit(y))
				map[loadcounter.x][loadcounter.y] = Vector2i(-1, -1);
			else
				map[loadcounter.x][loadcounter.y] = Vector2i(x - '0', y - '0');

			if (myfile.peek() == '\n')
				loadcounter.x = 0, loadcounter.y++;
			else
				loadcounter.x++;

		}

		loadcounter.y++;
	}


}

Sprite tilemaps::displaymap(int i, int j)
{

	for (i; i < loadcounter.x; i++) {
		for (j; j < loadcounter.y; j++)
		{
			if (map[i][j].x != -1 && map[i][j].y != -1)
			{
				tiles.setPosition(i * 32, j * 32);
				tiles.setTextureRect(IntRect(map[i][j].x * 32, map[i][j].y * 32, 32, 32));

			}
			tilesarray[i*loadcounter.y + j] = tiles;


			return tiles;
		}
	}
}

Sprite* tilemaps::gettiles()
{
	return tilesarray;
}

Vector2i tilemaps::getloadcounter()
{
	return loadcounter;
}