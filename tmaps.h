#pragma once

#include <SFML/Graphics.hpp>
#include<iostream>
#include<fstream>
#include<cctype>
#include<string>
using namespace std;
using namespace sf;
#pragma once
class tilemaps
{
public:
	tilemaps(int);
	Sprite displaymap(int, int);
	Vector2i getloadcounter();

	Sprite* gettiles();


	Texture tiletexture;
	Sprite tiles;

	Sprite* tilesarray = new Sprite[100];

	Vector2i map[100][100];
	Vector2i loadcounter = Vector2i(0, 0);

	string lname;
};

